﻿using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Core.Mapping
{
  public class PhotoPropertiesMappingProfile : Profile
  {
    public PhotoPropertiesMappingProfile()
    {
      CreateMap<PhotoProperties, PhotoPropertiesDto>();
      CreateMap<PhotoPropertiesDto, PhotoProperties>();
    }
  }
}
