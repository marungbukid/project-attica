﻿using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;
using System.Linq;

namespace Attica.Core.Mapping
{
  public class UserMappingProfile : Profile
  {
    public UserMappingProfile()
    {
      CreateMap<User, UserDto>()
        .ForMember(dto => dto.LikedPostsDto,
          opts => opts.MapFrom(u => u.LikedPosts));

      CreateMap<UserDto, User>();
    }

  }
}
