﻿using System;
using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;

namespace Attica.Core.Mapping
{
  public class PhotoMappingProfile : Profile
  {
    public PhotoMappingProfile()
    {
      CreateMap<Photo, PhotoDto>()
        .ForMember(dto => dto.Properties,
          opts => opts.MapFrom(p => p.PhotoProperties));

      CreateMap<PhotoDto, Photo>()
        .ForMember(p => p.PhotoProperties,
          opts => opts.MapFrom(dto => dto.Properties));
    }
  }
}
