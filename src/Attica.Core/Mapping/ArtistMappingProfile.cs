﻿using System.Linq;
using Attica.Core.Entries;
using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;

namespace Attica.Core.Mapping
{
  public class ArtistMappingProfile : Profile
  {
    public ArtistMappingProfile()
    {
      CreateMap<Artist, ArtistDto>()
        .ForMember(dr => dr.ArtistEntry,
          opts => 
            opts.MapFrom(d => 
              new ArtistEntry 
              { 
                IdentityId = d.IdentityId,
                ArtistCredentials = new ArtistCrendentialsEntry
                {
                  FirstName = d.FirstName,
                  LastName = d.LastName,
                  Bio = d.Bio,
                  Degree = d.Degree,
                  GraduatedFrom = d.GraduatedFrom,
                  AvatarFileName = d.AvatarFileName
                }
              }))
        .ForMember(dr => dr.PostResources, 
          opts => opts.MapFrom(d => d.Posts
            .Where(p => p.ArtistId == d.Id)
            .Select(p => new PostDto
            {
              Id = p.Id,
              PostDetails = new PostEntry
              {
                Title = p.Title,
                Price = p.Price,
                FinePrint = p.FinePrint,
                VicinitySize = p.VicinitySize,
                About = p.About
              }
            })));

      CreateMap<ArtistEntry, Artist>()
        .ForMember(a => a.FirstName,
          opts => opts.MapFrom(dto => dto.ArtistCredentials.FirstName))
        .ForMember(a => a.LastName,
          opts => opts.MapFrom(dto => dto.ArtistCredentials.LastName))
        .ForMember(a => a.Bio,
          opts => opts.MapFrom(dto => dto.ArtistCredentials.Bio))
        .ForMember(a => a.GraduatedFrom,
          opts => opts.MapFrom(dto => dto.ArtistCredentials.GraduatedFrom))
        .ForMember(a => a.Degree,
          opts => opts.MapFrom(dto => dto.ArtistCredentials.Degree))
        .ForMember(a => a.AvatarFileName,
          opts => opts.MapFrom(dto => dto.ArtistCredentials.AvatarFileName));
      CreateMap<ArtistDto, Artist>()
        .ForMember(d => d.Posts, opts => opts.Ignore());
    }
  }
}
