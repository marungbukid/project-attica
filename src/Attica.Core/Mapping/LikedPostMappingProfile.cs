﻿using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Core.Mapping
{
  public class LikedPostMappingProfile : Profile
  {
    public LikedPostMappingProfile()
    {
      CreateMap<LikedPost, LikedPostDto>()
        .ForMember(dto => dto.PostDto,
          opts => opts.MapFrom(lp => lp.Post));

      CreateMap<LikedPostDto, LikedPost>();
    }
  }
}
