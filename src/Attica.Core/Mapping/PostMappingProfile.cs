﻿using Attica.Core.Entries;
using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;

namespace Attica.Core.Mapping
{
  public class PostMappingProfile : Profile
  {
    public PostMappingProfile()
    {
      CreateMap<Post, PostDto>()
        .ForMember(pd => pd.PostDetails,
          opts => opts.MapFrom(p => new PostEntry
          {
            Title = p.Title,
            Price = p.Price,
            FinePrint = p.FinePrint,
            VicinitySize = p.VicinitySize,
            About = p.About
          }))
        .ForMember(pd => pd.ArtistDto,
          opts => opts.MapFrom(p => new ArtistDto
          {
            Id = p.ArtistId,
            ArtistEntry = new ArtistEntry
            {
              IdentityId = p.Artist.IdentityId,
              ArtistCredentials = new ArtistCrendentialsEntry
              {
                Bio = p.Artist.Bio,
                Degree = p.Artist.Degree,
                FirstName = p.Artist.FirstName,
                LastName = p.Artist.LastName,
                GraduatedFrom = p.Artist.GraduatedFrom,
                AvatarFileName = p.Artist.AvatarFileName
              }
            }
          }))
        .ForMember(pd => pd.PhotosDto,
          opts => opts.MapFrom(p => p.Photos));

      CreateMap<PostEntry, Post>();
      CreateMap<PostDto, Post>();

      CreateMap<UpdatePostEntry, Post>();
    }
  }
}
