﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Attica.Core.Entries;
using Attica.Core.Interfaces;
using Attica.Core.Interfaces.Repositories;
using Attica.Core.Interfaces.Services;
using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;

namespace Attica.Core.Services
{
  public class ArtistService : IArtistService
  {
    private readonly IMapper mapper;
    private readonly IUnitOfWork unitOfWork;
    private readonly IArtistRepository repository;

    public ArtistService(
      IMapper mapper,
      IUnitOfWork unitOfWork,
      IArtistRepository repository)
    {
      this.mapper = mapper;
      this.unitOfWork = unitOfWork;
      this.repository = repository;
    }

    public async Task<ArtistDto> AddArtistAsync(ArtistEntry artistEntry)
    {
      var artist = mapper.Map<Artist>(artistEntry);

      await repository.AddArtistAsync(artist);
      await unitOfWork.CompleteAsync();

      var result = mapper.Map<ArtistDto>(artist);

      return result;
    }

    public async Task<bool> DeleteArtistAsync(int id)
    {
      var artist = await repository.GetArtistAsync(id);
      if (artist == null)
        return false;

      repository.DeleteArtist(artist);
      await unitOfWork.CompleteAsync();

      return true;
    }

    public async Task<ArtistDto> GetArtistAsync(int id)
    {
      var artist = await repository.GetArtistAsync(id);
      var result = mapper.Map<ArtistDto>(artist);

      return result;
    }

    public async Task<ArtistDto> GetArtistByIdentityIdAsync(string identityId)
    {
      var artist = await repository.GetArtustByIdentityIdAsync(identityId);
      var result = mapper.Map<ArtistDto>(artist);

      return result;
    }

    public async Task<IEnumerable<ArtistDto>> GetArtistsAsync()
    {
      var artists = await repository.GetArtistsAsync();
      var result = mapper.Map<IEnumerable<ArtistDto>>(artists);

      return result;
    }

    public async Task<ArtistDto> UpdateArtistCredentialsAsync(int id, ArtistEntry artistEntry)
    {
      var artist = await repository.GetArtistAsync(id);
      if (artist == null)
        return null;

      artist.Bio = artistEntry.ArtistCredentials.Bio;
      artist.FirstName = artistEntry.ArtistCredentials.FirstName;
      artist.LastName = artistEntry.ArtistCredentials.LastName;
      artist.GraduatedFrom = artistEntry.ArtistCredentials.GraduatedFrom;
      artist.AvatarFileName = artistEntry.ArtistCredentials.AvatarFileName;

      repository.UpdateArtistDetails(artist);

      await unitOfWork.CompleteAsync();

      var result = mapper.Map<ArtistDto>(artist);

      return result;
    }
  }
}
