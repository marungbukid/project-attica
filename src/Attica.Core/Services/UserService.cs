﻿using Attica.Core.Interfaces;
using Attica.Core.Interfaces.Repositories;
using Attica.Core.Interfaces.Services;
using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attica.Core.Services
{
  public class UserService : IUserService
  {
    private readonly IMapper mapper;
    private readonly IUserRepository repository;
    private readonly IUnitOfWork unitOfWork;

    public UserService(
      IMapper mapper,
      IUserRepository repository,
      IUnitOfWork unitOfWork)
    {
      this.mapper = mapper;
      this.repository = repository;
      this.unitOfWork = unitOfWork;
    }

    public async Task<UserDto> AuthorizeUserAsync(string identityId)
    {
      var userDto = new UserDto
      {
        IdentityId = identityId
      };
      var exists = await repository.DoUserExistAsync(userDto.IdentityId);

      if (!exists)
      {
        var result = await repository.AuthorizeUserAsync(mapper.Map<User>(userDto));
        await unitOfWork.CompleteAsync();

        return mapper.Map<UserDto>(result);
      }

      return userDto;
    }

    public async Task<UserDto> GetUserByIdAsync(int id)
    {
      var user = await repository.GetUserByIdAsync(id);

      return mapper.Map<UserDto>(user);
    }

    public async Task<UserDto> GetUserByIdentityIdAsync(string identityId)
    {
      var user = await repository.GetUserByIdentityIdAsync(identityId);

      return mapper.Map<UserDto>(user);
    }

    public async Task<IEnumerable<UserDto>> GetUsersAsync()
    {
      var users = await repository.GetUsersAsync();

      return mapper.Map<IEnumerable<UserDto>>(users);
    }

  
    public async Task<UserDto> UpdateUserLikedPost(int id, IEnumerable<int> likedPostIds)
    {
      var user = await repository.GetUserByIdAsync(id);
      var likedPosts = likedPostIds.Select(id => new LikedPost { PostId = id });

      user.LikedPosts = likedPosts.ToList();

      repository.UpdateUser(user);
      await unitOfWork.CompleteAsync();

      return mapper.Map<UserDto>(user);
    }

  }
}
