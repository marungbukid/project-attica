﻿using Attica.Core.Interfaces;
using Attica.Persistence.DbContexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attica.Core.Services
{
  public class UnitOfWork : IUnitOfWork
  {
    private readonly AtticaDbContext context;

    public UnitOfWork(
      AtticaDbContext context)
    {
      this.context = context;
    }

    public async Task CompleteAsync()
    {
      await context.SaveChangesAsync();
    }
  }
}
