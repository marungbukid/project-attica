﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attica.Core.Entries;
using Attica.Core.Interfaces;
using Attica.Core.Interfaces.Repositories;
using Attica.Core.Interfaces.Services;
using Attica.Core.Resources;
using Attica.Persistence.Models;
using AutoMapper;

namespace Attica.Core.Services
{
  public class PostService : IPostService
  {
    private readonly IMapper mapper;
    private readonly IPostRepository repository;
    private readonly IArtistRepository artistRepository;
    private readonly IUnitOfWork unitOfWork;

    public PostService(
      IMapper mapper,
      IPostRepository repository,
      IArtistRepository artistRepository,
      IUnitOfWork unitOfWork)
    {
      this.mapper = mapper;
      this.repository = repository;
      this.artistRepository = artistRepository;
      this.unitOfWork = unitOfWork;
    }

    public async Task<PostDto> AddPostAsync(int artistId, PostEntry postEntry)
    {
      var post = mapper.Map<Post>(postEntry);
      var artist = await artistRepository.GetArtistAsync(artistId, includePosts: false);

      if (artist == null)
        return null;

      post.Artist = artist;

      await repository.AddPostAsync(post);
      await unitOfWork.CompleteAsync();

      var result = mapper.Map<PostDto>(post);

      return result;
    }

    public async Task<bool> DeletePostAsync(int postId)
    {
      var post = await repository.GetPostAsync(postId);
      if (post == null)
        return false;

      repository.DeletePost(post);
      await unitOfWork.CompleteAsync();

      return true;
    }

    public async Task<PostDto> GetPostAsync(int postId)
    {
      var post = await repository.GetPostAsync(postId);
      post.Views += 1;

      repository.UpdatePost(post);
      await unitOfWork.CompleteAsync();

      return mapper.Map<PostDto>(post);
    }

    public async Task<IEnumerable<PostDto>> GetPostsByArtistIdAsync(int artistId)
    {
      var posts = await repository.GetPostsByArtistIdAsync(artistId);

      return mapper.Map<IEnumerable<PostDto>>(posts);
    }

    public async Task<IEnumerable<PostDto>> GetPostsAsync()
    {
      var posts = await repository.GetPostsAsync();

      return mapper.Map<IEnumerable<PostDto>>(posts);
    }

    public async Task<PostDto> AddPostPhotoAsync(int id, PhotoDto photoDto)
    {
      var post = await repository.GetPostAsync(id);
      var photo = mapper.Map<Photo>(photoDto);

      if (photoDto.Properties.IsFeatured)
      {
        var featuredPhotos = post.Photos
          .Where(ph => ph.PhotoProperties != null && ph.PhotoProperties.IsFeatured)
          .ToList();

        foreach (var featuredPhoto in featuredPhotos)
        {
          post.Photos.Remove(featuredPhoto);
        }

        repository.UpdatePost(post);
        await unitOfWork.CompleteAsync();
      }

      post.Photos.Add(photo);

      repository.UpdatePost(post);
      await unitOfWork.CompleteAsync();

      return mapper.Map<PostDto>(post);
    }

    public async Task<PostDto> UpdatePostEntryAsync(int postId, PostEntry postEntry)
    {
      var post = await repository.GetPostAsync(postId);
      if (post == null)
        return null;

      post.Title = postEntry.Title;
      post.FinePrint = postEntry.FinePrint;
      post.VicinitySize = postEntry.VicinitySize;
      post.Price = postEntry.Price;

      repository.UpdatePost(post);

      await unitOfWork.CompleteAsync();

      var result = mapper.Map<PostDto>(post);

      return result;
    }

    public async Task<PostDto> AddPostMultiplePhotoAsync(int id, IEnumerable<PhotoDto> photos)
    {
      var post = await repository.GetPostAsync(id);

      foreach (var photoDto in photos)
      {
        var photo = mapper.Map<Photo>(photoDto);
        post.Photos.Add(photo);
      }


      repository.UpdatePost(post);
      await unitOfWork.CompleteAsync();

      return mapper.Map<PostDto>(post);
    }

    public async Task<PostDto> UpdatePostAsync(UpdatePostEntry updatePostEntry)
    {
      var post = mapper.Map<Post>(updatePostEntry);

      repository.UpdatePost(post);
      await unitOfWork.CompleteAsync();

      return mapper.Map<PostDto>(post);
    }
  }
}
