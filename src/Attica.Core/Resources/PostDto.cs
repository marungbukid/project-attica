﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Attica.Core.Entries;

namespace Attica.Core.Resources
{
  public class PostDto
  {
    public int Id { get; set; }
    public long Views { get; set; }
    public PostEntry PostDetails { get; set; }
    public ArtistDto ArtistDto { get; set; }
    public ICollection<PhotoDto> PhotosDto { get; set; }

    public PostDto()
    {
      PhotosDto = new Collection<PhotoDto>();
    }
  }
}
