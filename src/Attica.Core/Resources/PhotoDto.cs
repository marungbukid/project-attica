﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Attica.Core.Resources
{
  public class PhotoDto
  {
    [Required]
    [StringLength(255)]
    public string FileName { get; set; }
    public PhotoPropertiesDto Properties { get; set; }
  }
}
