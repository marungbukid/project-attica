﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Core.Resources
{
  public class PhotoPropertiesDto
  {
    public bool IsFeatured { get; set; }
  }
}
