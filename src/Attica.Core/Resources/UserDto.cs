﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Attica.Core.Resources
{
  public class UserDto
  {
    public int Id { get; set; }
    public string IdentityId { get; set; }

    public ICollection<LikedPostDto> LikedPostsDto { get; set; }

    public UserDto()
    {
      LikedPostsDto = new Collection<LikedPostDto>();
    }
  }
}
