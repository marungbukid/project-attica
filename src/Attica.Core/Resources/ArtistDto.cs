﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.Json.Serialization;
using Attica.Core.Entries;

namespace Attica.Core.Resources
{
  public class ArtistDto
  {
    public int Id { get; set; }
    
    [JsonPropertyName("artistDetails")]
    public ArtistEntry ArtistEntry { get; set; }

    [JsonPropertyName("posts")]
    public ICollection<PostDto> PostResources { get; set; }

    public ArtistDto()
    {
      PostResources = new Collection<PostDto>();
    }
  }
}
