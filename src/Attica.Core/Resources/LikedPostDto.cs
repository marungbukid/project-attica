﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Core.Resources
{
  public class LikedPostDto
  {
    public int UserId { get; set; }
    public int PostId { get; set; }

    public UserDto UserDto { get; set; }
    public PostDto PostDto { get; set; }
  }
}
