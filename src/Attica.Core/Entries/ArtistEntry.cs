﻿using System.ComponentModel.DataAnnotations;

namespace Attica.Core.Entries
{
  public class ArtistEntry
  {
    public string IdentityId { get; set; }

    public ArtistCrendentialsEntry ArtistCredentials { get; set; }
  }
}
