﻿using Attica.Core.Resources;

namespace Attica.Core.Entries
{
  public class PostEntry
  {
    public string Title { get; set; }
    public double Price { get; set; }
    public string FinePrint { get; set; }
    public string About { get; set; }
    public float VicinitySize { get; set; }
  }
}
