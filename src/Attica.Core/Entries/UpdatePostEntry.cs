﻿using Attica.Core.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Attica.Core.Entries
{
  public class UpdatePostEntry
  {
    public int Id { get; set; }
    public PostEntry PostDetails { get; set; }
    public ICollection<PhotoDto> PhotosDto { get; set; }

    public UpdatePostEntry()
    {
      PhotosDto = new Collection<PhotoDto>();
    }
  }
}
