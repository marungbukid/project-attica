﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Attica.Core.Entries
{
  public class ArtistCrendentialsEntry
  {
    [Required]
    [StringLength(100)]
    public string FirstName { get; set; }

    [Required]
    [StringLength(100)]
    public string LastName { get; set; }

    [Required]
    [StringLength(255)]
    public string Bio { get; set; }

    [Required]
    [StringLength(100)]
    public string GraduatedFrom { get; set; }

    [Required]
    [StringLength(255)]
    public string Degree { get; set; }

    public string AvatarFileName { get; set; }
  }
}
