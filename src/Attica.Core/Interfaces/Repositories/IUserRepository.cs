﻿using Attica.Core.Resources;
using Attica.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attica.Core.Interfaces.Repositories
{
  public interface IUserRepository
  {
    #region Default User Procedures
    Task<IEnumerable<User>> GetUsersAsync();
    Task<User> GetUserByIdAsync(int id);
    void UpdateUser(User user);
    #endregion

    #region Authorization
    Task<User> GetUserByIdentityIdAsync(string identityId);
    Task<User> AuthorizeUserAsync(User user);
    Task<bool> DoUserExistAsync(string identityId);
    #endregion
  }
}
