﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Attica.Persistence.Models;

namespace Attica.Core.Interfaces.Repositories
{
  public interface IPostRepository
  {
    Task<Post> GetPostAsync(int postId);
    Task<IEnumerable<Post>> GetPostsByArtistIdAsync(int artistId);
    Task<IEnumerable<Post>> GetPostsAsync();
    Task AddPostAsync(Post post);
    void DeletePost(Post post);
    void UpdatePost(Post post);  
  }
}
