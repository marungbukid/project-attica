﻿using Attica.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attica.Core.Interfaces.Repositories
{
  public interface IArtistRepository
  {
    Task<IEnumerable<Artist>> GetArtistsAsync();
    Task<Artist> GetArtistAsync(int id, bool includePosts = true);
    Task<Artist> GetArtustByIdentityIdAsync(string identityId);
    Task AddArtistAsync(Artist artist);
    void UpdateArtistDetails(Artist artist);
    void DeleteArtist(Artist artist);
  }
}
