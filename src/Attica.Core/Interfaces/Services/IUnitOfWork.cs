﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attica.Core.Interfaces
{
  public interface IUnitOfWork
  {
    Task CompleteAsync();
  }
}
