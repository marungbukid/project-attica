﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Attica.Core.Entries;
using Attica.Core.Resources;

namespace Attica.Core.Interfaces.Services
{
  public interface IPostService
  {
    Task<PostDto> GetPostAsync(int postId);
    Task<IEnumerable<PostDto>> GetPostsByArtistIdAsync(int artistId);
    Task<IEnumerable<PostDto>> GetPostsAsync();
    Task<PostDto> AddPostAsync(int artistId, PostEntry postEntry);
    Task<PostDto> UpdatePostAsync(UpdatePostEntry updatePostEntry);
    Task<PostDto> UpdatePostEntryAsync(int postId, PostEntry postEntry);
    Task<bool> DeletePostAsync(int postId);
    Task<PostDto> AddPostPhotoAsync(int id, PhotoDto photoDto);
    Task<PostDto> AddPostMultiplePhotoAsync(int id, IEnumerable<PhotoDto> photos);
  }
}
