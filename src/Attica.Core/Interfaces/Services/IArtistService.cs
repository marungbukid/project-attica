﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Attica.Core.Entries;
using Attica.Core.Resources;

namespace Attica.Core.Interfaces.Services
{
  public interface IArtistService
  {
    Task<IEnumerable<ArtistDto>> GetArtistsAsync();
    Task<ArtistDto> GetArtistAsync(int id);
    Task<ArtistDto> GetArtistByIdentityIdAsync(string identityId);
    Task<ArtistDto> AddArtistAsync(ArtistEntry artistEntry);
    Task<ArtistDto> UpdateArtistCredentialsAsync(int id, ArtistEntry artistEntry);
    Task<bool> DeleteArtistAsync(int id);
  }
}
