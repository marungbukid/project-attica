﻿using Attica.Core.Resources;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attica.Core.Interfaces.Services
{
  public interface IUserService
  {
    #region Default User Procedures
    Task<IEnumerable<UserDto>> GetUsersAsync();
    Task<UserDto> GetUserByIdAsync(int id);
    Task<UserDto> UpdateUserLikedPost(int id, IEnumerable<int> likedPostIds);
    #endregion

    #region Authorization
    Task<UserDto> AuthorizeUserAsync(string identityId);
    Task<UserDto> GetUserByIdentityIdAsync(string identityId);
    #endregion

  }
}
