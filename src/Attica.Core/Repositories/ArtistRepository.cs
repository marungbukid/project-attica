﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attica.Core.Interfaces.Repositories;
using Attica.Persistence.DbContexts;
using Attica.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace Attica.Core.Repositories
{
  public class ArtistRepository : IArtistRepository
  {
    private readonly AtticaDbContext context;

    public ArtistRepository(
      AtticaDbContext context)
    {
      this.context = context;
    }

    public async Task AddArtistAsync(Artist artist)
    {
      await context.Artists.AddAsync(artist);
    }

    public void DeleteArtist(Artist artist)
    {
      context.Artists.Remove(artist);
    }

    public async Task<Artist> GetArtistAsync(int id, bool includePosts = true)
    {
      if (!includePosts)
        return await context.Artists
          .Where(d => d.Id == id)
          .FirstOrDefaultAsync();

      return await context.Artists
        .Where(d => d.Id == id)
          .Include(d => d.Posts)
        .FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Artist>> GetArtistsAsync()
    {
      return await context.Artists
          .Include(d => d.Posts)
        .ToListAsync();
    }

    public async Task<Artist> GetArtustByIdentityIdAsync(string identityId)
    {
      return await context.Artists
        .Where(a => a.IdentityId == identityId)
          .Include(a => a.Posts)
        .FirstOrDefaultAsync();
    }

    public void UpdateArtistDetails(Artist artist)
    {
      context.Artists.Update(artist);
    }
  }
}
