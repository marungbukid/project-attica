﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attica.Core.Interfaces.Repositories;
using Attica.Persistence.DbContexts;
using Attica.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Attica.Core.Repositories
{
  public class PostRepository : IPostRepository
  {
    private readonly AtticaDbContext context;

    public PostRepository(
      AtticaDbContext context)
    {
      this.context = context;
    }

    public async Task AddPostAsync(Post post)
    {
      await context.Posts.AddAsync(post);
    }

    public void DeletePost(Post post)
    {
      context.Posts.Remove(post);
    }

    public async Task<Post> GetPostAsync(int postId)
    {
      return await context.Posts
        .Where(p => p.Id == postId)
          .Include(p => p.Photos)
            .ThenInclude(ph => ph.PhotoProperties)
          .Include(p => p.Artist)
        .FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Post>> GetPostsByArtistIdAsync(int artistId)
    {
      return await context.Posts
        .Where(p => p.ArtistId == artistId)
          .Include(p => p.Photos)
            .ThenInclude(ph => ph.PhotoProperties)
        .ToListAsync();
    }

    public async Task<IEnumerable<Post>> GetPostsAsync()
    {
      return await context.Posts
        .IncludeFilter(p => p.Photos.Select(ph => ph.PhotoProperties).Where(prop => prop.IsFeatured))
        .IncludeFilter(p => p.Photos.Where(ph => ph.PhotoProperties.IsFeatured))
        .IncludeFilter(p => p.Artist)
        .ToListAsync();
    }

    public void UpdatePost(Post post)
    {
      context.Posts.Update(post);
    }
  }
}
