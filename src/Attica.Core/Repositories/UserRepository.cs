﻿using Attica.Core.Interfaces.Repositories;
using Attica.Core.Resources;
using Attica.Persistence.DbContexts;
using Attica.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Attica.Core.Repositories
{
  public class UserRepository : IUserRepository
  {
    private readonly AtticaDbContext context;

    public UserRepository(
      AtticaDbContext context)
    {
      this.context = context;
    }

    public async Task<bool> DoUserExistAsync(string identityId)
    {
      return (await context.Users
          .AsNoTracking()
          .Where(u => u.IdentityId == identityId)
          .CountAsync()) > 0;
    }

    public async Task<User> GetUserByIdentityIdAsync(string identityId)
    {
      return await context.Users
          .Where(u => u.IdentityId == identityId)
          .FirstOrDefaultAsync();
    }

    public async Task<User> AuthorizeUserAsync(User user)
    {
      await context.Users
       .AddAsync(user);

      return user;
    }

    public async Task<IEnumerable<User>> GetUsersAsync()
    {
      var result = await context.Users
        .ToListAsync();

      return result;
    }

    public async Task<User> GetUserByIdAsync(int id)
    {
      var result = await context.Users
          //.Include(u => u.LikedPosts)
          //  .ThenInclude(lp => lp.Post)
          //  .ThenInclude(p => p.Photos)
          //    .ThenInclude(ph => ph.PhotoProperties)
          //.Include(u => u.LikedPosts)
          //  .ThenInclude(lp => lp.Post)
          //  .ThenInclude(p => p.Artist)

          .IncludeFilter(u => u.LikedPosts)
            .IncludeFilter(u => u.LikedPosts.Select(l => l.Post))
          .Where(u => u.Id == id)
          .FirstOrDefaultAsync();

      return result; 
    }

    public void UpdateUser(User user)
    {
      context.Users.Update(user);
    }
  }
}
