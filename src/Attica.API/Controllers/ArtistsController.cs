﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Attica.API.Configuration;
using Attica.Core.Entries;
using Attica.Core.Interfaces.Services;
using Attica.Core.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Attica.API.Controllers
{
  [Authorize]
  [Route("api/[controller]")]
  public class ArtistsController : Controller
  {
    private readonly IArtistService service;
    private readonly IHostEnvironment hostEnvironment;
    private readonly PhotoSettings photoSettings;

    public ArtistsController(
      IArtistService service,
      IHostEnvironment hostEnvironment,
      IOptionsSnapshot<PhotoSettings> options)
    {
      this.service = service;
      this.hostEnvironment = hostEnvironment;
      photoSettings = options.Value;
    }

    [HttpGet]
    public async Task<IEnumerable<ArtistDto>> GetArtistsAsync()
    {
      return await service.GetArtistsAsync();
    }

    [HttpGet("{id}")]
    public async Task<ArtistDto> GetArtistAsync(int id)
    {
      return await service.GetArtistAsync(id);
    }

    [HttpGet("identity/{identityId}")]
    public async Task<ArtistDto> GetArtistByIdentityIdAsync(string identityId)
    {
      var artist = await service.GetArtistByIdentityIdAsync(identityId);

      return artist;
    }

    [HttpPost]
    public async Task<IActionResult> AddArtistAsync([FromBody]ArtistEntry artistEntry)
    {
      var existingArtist = await service.GetArtistByIdentityIdAsync(artistEntry.IdentityId);
      if (existingArtist != null)
      {
        return await UpdateArtistCredentialsAsync(existingArtist.Id, artistEntry);
      }

      var artist = await service.AddArtistAsync(artistEntry);

      return Ok(artist);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateArtistCredentialsAsync(int id, [FromBody]ArtistEntry artistEntry)
    {
      var artist = await service.UpdateArtistCredentialsAsync(id, artistEntry);
      if (artist == null)
        return NotFound();

      return Ok(artist);
    }


    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteArtistAsync(int id)
    {
      var result = await service.DeleteArtistAsync(id);
      if (!result)
        return NotFound();
      return Ok();
    }

    [HttpPost("{id}/avatar")]
    public async Task<IActionResult> UploadAvatarAsync(int id, IFormFile file)
    {
      var artist = await service.GetArtistAsync(id);
      if (artist == null)
        return NotFound();

      var fileExtension = Path.GetExtension(file.FileName);

      if (file == null) return BadRequest("No file uploaded");
      if (file.Length == 0) return BadRequest("No file uploaded");

      if (photoSettings.IsFileSizeExceeded(file.Length)) return BadRequest("Max file size exceeded");
      if (photoSettings.IsInvalidFileType(fileExtension)) return BadRequest("Invalid file type");

      var fileName = Guid.NewGuid().ToString() + fileExtension;
      var uploadsFolderPath = Path.Combine(
        hostEnvironment.ContentRootPath,
        photoSettings.UploadsFolder);

      if (!Directory.Exists(uploadsFolderPath))
        Directory.CreateDirectory(uploadsFolderPath);

      var filePath = Path.Combine(uploadsFolderPath, fileName);

      using var stream = new FileStream(filePath, FileMode.Create);
      await file.CopyToAsync(stream);

      artist.ArtistEntry.ArtistCredentials.AvatarFileName = fileName;
      await service.UpdateArtistCredentialsAsync(id, artist.ArtistEntry);

      return Ok(artist);
    }

  }
}