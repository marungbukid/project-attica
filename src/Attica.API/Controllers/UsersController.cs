﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attica.Core.Interfaces.Services;
using Attica.Core.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Attica.API.Controllers
{
  [Authorize]
  [Route("/api/[controller]")]
  public class UsersController : Controller
  {
    private readonly IUserService service;

    public UsersController(
      IUserService service)
    {
      this.service = service;
    }

    #region Default User procedures

    [HttpGet]
    public async Task<IActionResult> GetUsersAsync()
    {
      var result = await service.GetUsersAsync();
      if (!result.Any())
        return NotFound();

      return Ok(result);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetUserByIdAsync(int id)
    {
      var result = await service.GetUserByIdAsync(id);
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    #endregion





    #region Authorize
    [HttpGet("authorize/{identityId}")]
    public async Task<IActionResult> GetUserAsync(string identityId)
    {
      var result = await service.GetUserByIdentityIdAsync(identityId);
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    [HttpPost("authorize/{identityId}")]
    public async Task<IActionResult> AuthorizeIdentityIdAsync(string identityId)
    {
      var result = await service.AuthorizeUserAsync(identityId);
      if (result == null)
        return BadRequest();

      return Ok(result);
    }

    #endregion


    [HttpPut("like/{id}")]
    public async Task<IActionResult> LikePostAsync(int id, [FromBody]IEnumerable<int> likedPostsIds)
    {
      var result = await service.UpdateUserLikedPost(id, likedPostsIds);
      if (result == null)
        return NotFound();

      return Ok(result);
    }
  }
}