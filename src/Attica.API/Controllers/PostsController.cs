﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Attica.API.Configuration;
using Attica.Core.Entries;
using Attica.Core.Interfaces.Services;
using Attica.Core.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Attica.API.Controllers
{
  [Authorize]
  [Route("api/[controller]")]
  public class PostsController : Controller
  {
    private readonly IHostEnvironment hostEnvironment;
    private readonly IPostService service;
    private readonly PhotoSettings photoSettings;

    public PostsController(
      IHostEnvironment hostEnvironment,
      IPostService service,
      IOptionsSnapshot<PhotoSettings> options)
    {
      this.hostEnvironment = hostEnvironment;
      this.service = service;
      photoSettings = options.Value;
    }

    [HttpGet]
    public async Task<IActionResult> GetPosts()
    {
      IEnumerable<PostDto> result = await service.GetPostsAsync();
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetPostAsync(int id)
    {
      PostDto result = await service.GetPostAsync(id);
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    [HttpGet("artist")]
    public async Task<IActionResult> GetPostAsyncByArtistId([FromQuery]int artistId)
    {
      var result = await service.GetPostsByArtistIdAsync(artistId);
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    [HttpPost("{artistId}")]
    public async Task<IActionResult> AddPostAsync(int artistId, [FromBody]PostEntry postEntry)
    {
      PostDto result = await service.AddPostAsync(artistId, postEntry);
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    [HttpPut("details/{id}")]
    public async Task<IActionResult> UpdatePostDetailsAsync(int id, [FromBody]PostEntry postEntry)
    {
      PostDto result = await service.UpdatePostEntryAsync(id, postEntry);
      if (result == null)
        return NotFound();

      return Ok(result);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePostAsync(int id)
    {
      var result = await service.DeletePostAsync(id);
      if (!result)
        return NotFound();

      return Ok();
    }

    // TODO: Add Authorization if artist
    [HttpPost("photo/featured/{id}")]
    public async Task<IActionResult> UploadFeaturedPhotoAsync(int id, IFormFile file)
    {
      var post = await service.GetPostAsync(id);
      if (post == null)
        return NotFound();

      var fileExtension = Path.GetExtension(file.FileName);

      if (file == null) return BadRequest("No file uploaded");
      if (file.Length == 0) return BadRequest("No file uploaded");

      if (photoSettings.IsFileSizeExceeded(file.Length)) return BadRequest("Max file size exceeded");
      if (photoSettings.IsInvalidFileType(fileExtension)) return BadRequest("Invalid file type");

      var fileName = Guid.NewGuid().ToString() + fileExtension;

      var photoDto = new PhotoDto
      {
        FileName = fileName,
        Properties = new PhotoPropertiesDto
        {
          IsFeatured = true
        }
      };

      var result = await service.AddPostPhotoAsync(id, photoDto);

      await CopyPhotoAsync(fileName, file);

      return Ok(result);
    }

    [HttpPost("photo/{id}")]
    public async Task<IActionResult> UploadGalleryPhotoAsync(int id, IFormFileCollection files)
    {
      var post = await service.GetPostAsync(id);
      if (post == null)
        return NotFound();

      ICollection<PhotoDto> photoDtos = new Collection<PhotoDto>();

      foreach (var file in files)
      {
        var fileExtension = Path.GetExtension(file.FileName);

        if (file == null) return BadRequest("No file uploaded");
        if (file.Length == 0) return BadRequest("No file uploaded");

        if (photoSettings.IsFileSizeExceeded(file.Length)) return BadRequest("Max file size exceeded");
        if (photoSettings.IsInvalidFileType(fileExtension)) return BadRequest("Invalid file type");

        var fileName = Guid.NewGuid().ToString() + fileExtension;

        var photoDto = new PhotoDto
        {
          FileName = fileName,
          Properties = new PhotoPropertiesDto
          {
            IsFeatured = false
          }
        };

        photoDtos.Add(photoDto);

        await CopyPhotoAsync(fileName, file);
      }

      var result = await service.AddPostMultiplePhotoAsync(id, photoDtos);

      return Ok(result);
    }

    // TODO: ImageWriter
    private async Task CopyPhotoAsync(string fileName, IFormFile file)
    {
      var uploadsFolderPath = Path.Combine(
        hostEnvironment.ContentRootPath,
        photoSettings.UploadsFolder);

      if (!Directory.Exists(uploadsFolderPath))
        Directory.CreateDirectory(uploadsFolderPath);

      var filePath = Path.Combine(uploadsFolderPath, fileName);

      using var fileStream = new FileStream(filePath, FileMode.Create);
      await file.CopyToAsync(fileStream);
    }

  }
}
