﻿using System;
using System.Linq;

namespace Attica.API.Configuration
{
  public class PhotoSettings
  {
    public int MaxBytes { get; set; }
    public string UploadsFolder { get; set; }
    public string[] AcceptedFileTypes { get; set; }

    public bool IsFileSizeExceeded(long fileSize) => fileSize > MaxBytes;

    public bool IsInvalidFileType(string extension) =>
      !AcceptedFileTypes.Any(s => s == extension.ToLower());
  }
}
