using Attica.API.Configuration;
using Attica.Core.Interfaces;
using Attica.Core.Interfaces.Repositories;
using Attica.Core.Interfaces.Services;
using Attica.Core.Mapping;
using Attica.Core.Repositories;
using Attica.Core.Services;
using Attica.Persistence.DbContexts;
using AutoMapper;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Attica.API
{
  public class Startup
  {
    public Startup(IWebHostEnvironment environment)
    {
      Environment = environment;

      var builder = new ConfigurationBuilder()
        .SetBasePath(environment.ContentRootPath)
        .AddJsonFile("appsettings.json", optional: true)
        .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true)
        .AddEnvironmentVariables();

      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }
    public IWebHostEnvironment Environment { get; }


    // TODO: 
    // 1) Segragate service procedures
    // 2) Segragate Dependency Injection procedures
    public void ConfigureServices(IServiceCollection services)
    {
      services.Configure<PhotoSettings>(Configuration.GetSection("PhotosUploadSettings"));

      ///
      /// Resource Mappers
      ///
      services.AddAutoMapper(
        typeof(ArtistMappingProfile),
        typeof(PostMappingProfile),
        typeof(PhotoMappingProfile),
        typeof(UserMappingProfile),
        typeof(LikedPostMappingProfile),
        typeof(PhotoPropertiesMappingProfile)
      );


      ///
      /// Db Contexts
      ///
      services.AddDbContext<AtticaDbContext>(options =>
       options.UseSqlite(Configuration.GetConnectionString("DefaultConnection"),
        b => b.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name))
      );

      ///
      /// Services and Repositories
      ///
      services.AddScoped<IArtistService, ArtistService>();
      services.AddScoped<IArtistRepository, ArtistRepository>();

      services.AddScoped<IPostService, PostService>();
      services.AddScoped<IPostRepository, PostRepository>();

      services.AddScoped<IUserService, UserService>();
      services.AddScoped<IUserRepository, UserRepository>();

      services.AddScoped<IUnitOfWork, UnitOfWork>();


      ///
      /// Configurations
      ///
      services.AddCors(options =>
      {
        options.AddPolicy("AllowAllOrigins",
          builder =>
          {
            builder
              .AllowCredentials()
              .WithOrigins(
                "https://localhost:44365")
              .SetIsOriginAllowedToAllowWildcardSubdomains()
              .AllowAnyHeader()
              .AllowAnyMethod();
          });
      });

      services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
          .AddIdentityServerAuthentication(options =>
          {
            options.Authority = "https://localhost:44373";
            options.ApiName = "project-attica-api";
            options.ApiSecret = "attica";
          });

      services.AddAuthorization(options =>
      {
      });

      services.AddRouting(options =>
         {
           options.LowercaseUrls = true;
           options.LowercaseQueryStrings = true;
         });

      services.AddControllers()
        .AddJsonOptions(options =>
        {
          options.JsonSerializerOptions.IgnoreNullValues = true;
        });

      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "Api", Version = "v0" });

        c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
          Type = SecuritySchemeType.OAuth2,

          Flows = new OpenApiOAuthFlows
          {
            Implicit = new OpenApiOAuthFlow
            {
              AuthorizationUrl = new Uri("https://localhost:44373/connect/authorize"),
              Scopes =
              {
                { "project-attica-api", "Attica API Resource access" }
              }
            }
          }
        });

        c.OperationFilter<AuthorizeCheckOperationFilter>();
      });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      app.UseStaticFiles();

      app.UseSwagger();
      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v0");

        c.OAuthClientId("project-attica-swagger");

        c.RoutePrefix = string.Empty;
      });

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseHttpsRedirection();

      app.UseRouting();

      app.UseAuthentication();
      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }


  public class AuthorizeCheckOperationFilter : IOperationFilter
  {
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
      var hasAuthorize =
        context.MethodInfo
          .DeclaringType
          .GetCustomAttributes(true)
          .OfType<AuthorizeAttribute>()
          .Any();

      if (hasAuthorize)
      {
        operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
        operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden" });

        operation.Security = new List<OpenApiSecurityRequirement>
        {
          new OpenApiSecurityRequirement
          {
            {
              new OpenApiSecurityScheme
              {
                  Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
              },
              new[] { "project-attica-api" }
            }
          }
        };
      }
    }
  }

}
