﻿using Attica.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Persistence.DbContexts
{
  public class AtticaDbContext : DbContext
  {
    private readonly ILoggerFactory loggerFactory;

    public DbSet<Artist> Artists { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Photo> Photos { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<LikedPost> LikedPosts { get; set; }
    public DbSet<PhotoProperties> PhotoProperties { get; set; }

    public AtticaDbContext(DbContextOptions<AtticaDbContext> options, ILoggerFactory loggerFactory)
      : base(options)
    {
      this.loggerFactory = loggerFactory;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseLoggerFactory(loggerFactory);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<LikedPost>()
        .HasKey(lp =>
          new { lp.UserId, lp.PostId });
    }
  }
}
