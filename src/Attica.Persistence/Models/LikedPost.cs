﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Persistence.Models
{
  public class LikedPost
  {
    public int UserId { get; set; }
    public int PostId { get; set; }

    public User User { get; set; }
    public Post Post { get; set; }

  }
}
