﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Attica.Persistence.Models
{
  public class PhotoProperties
  {
    public int Id { get; set; }
    public int PhotoId { get; set; }
    public bool IsFeatured { get; set; }
  }
}
