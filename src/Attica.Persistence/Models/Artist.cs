﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Attica.Persistence.Models
{
  public class Artist : User
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Bio { get; set; }
    public string GraduatedFrom { get; set; }
    public string Degree { get; set; }
    public string AvatarFileName { get; set; }
    public ICollection<Post> Posts { get; set; }

    public Artist()
    {
      Posts = new Collection<Post>();
    }
  }
}
