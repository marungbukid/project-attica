﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Attica.Persistence.Models
{
  public class User
  {
    public int Id { get; set; }
    public string IdentityId { get; set; }
    public ICollection<LikedPost> LikedPosts { get; set; }

    public User()
    {
      LikedPosts = new Collection<LikedPost>();
    }
  }
}
