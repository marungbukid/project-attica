﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Attica.Persistence.Models
{
  public class Post
  {
    public int Id { get; set; }
    public int ArtistId { get; set; }
    public int HeaderId { get; set; }
    public string Title { get; set; }
    public double Price { get; set; }
    public string FinePrint { get; set; }
    public string About { get; set; }
    public float VicinitySize { get; set; }
    public long Views { get; set; }
    public Artist Artist { get; set; }
    public ICollection<Photo> Photos { get; set; }

    public Post()
    {
      Photos = new Collection<Photo>();
    }
  }
}
